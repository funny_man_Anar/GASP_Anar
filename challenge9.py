from random import randint

for i in range(10):
    num1 = randint(1,10)
    num2 = randint(1,10)
    Question = "What is " + str(num1) + " times " + str(num2) + "? "
    Answer = int(input(Question))
    if Answer == num1 * num2:
        print("That's right - well done.")
    else:
        print("No, I'm afraid the answer is " + str(num1 * num2) + ".")
